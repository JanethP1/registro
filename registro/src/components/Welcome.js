import React from 'react';

const Welcome = () => (
  <div>
    <h1>¡Bienvenido!</h1>
    <p>Regrístrate para ingresar</p>
    <button>Continuar<i class="fas fa-chevron-right"></i></button>
  </div>
);

export default Welcome;
